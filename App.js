import 'react-native-gesture-handler';
import * as React from 'react';
import Routes from './src/navigation/Routes';

//Authentication
import { withAuthenticator } from 'aws-amplify-react-native'
import { Auth } from 'aws-amplify';

import { Provider as StoreProvider } from 'react-redux'
import configureStore from './src/redux/configureStore'
import { PersistGate } from "redux-persist/integration/react";

import { LogBox } from 'react-native';

LogBox.ignoreAllLogs();

const { persistor, store } = configureStore();

class App extends React.Component {
	render() {
		return(
			<StoreProvider store={store}>
				<PersistGate loading={null} persistor={persistor}>
					<Routes/>
				</PersistGate>
			</StoreProvider>
		)
	}
}

export default App;