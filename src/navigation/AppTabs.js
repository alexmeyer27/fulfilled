import React, { useContext } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Text } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import HomeStack from './stacks/HomeStack';
import { ConfigStack } from './stacks/ConfigStack';
import { SettingsStack } from './stacks/SettingsStack';
import { DepartmentStack } from './stacks/DepartmentStack';
import InventoryStack from './stacks/InventoryStack';

const Tabs = createBottomTabNavigator()


class AppTabs extends React.Component {
	render(){
		return (
			<Tabs.Navigator 
				screenOptions={({ route }) => ({
					tabBarIcon: ({ focused, color, size }) => {
					let iconName;
	
					if (route.name === 'Home') {
						iconName = 'ios-home'
					} else if (route.name === 'Configuration') {
						iconName = 'profile';
						return <AntDesign name={iconName} size={size} color={color} />;
					} else if (route.name === 'Inventory') {
						iconName = 'book';
						return <AntDesign name={iconName} size={size} color={color} />;
					} else if (route.name === 'Settings') {
						iconName = 'md-settings'
					} else if (route.name === 'Department') {
						iconName = 'cube-send'
						return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
					} else {
						console.log(route.name)
					}
	
					return <Ionicons name={iconName} size={size} color={color} />;
					},
				})}
				tabBarOptions={{
					activeTintColor: 'tomato',
					inactiveTintColor: 'gray',
				}}
			>
				<Tabs.Screen name='Home' component={HomeStack} screenOptions={{header: null}}/>
				<Tabs.Screen name='Department' component={DepartmentStack}/>
				<Tabs.Screen name='Inventory' component={InventoryStack}/>  
				<Tabs.Screen name='Configuration' component={ConfigStack}/>
				{/* <Tabs.Screen name='Orders' component={OrdersStack}/> */}
				<Tabs.Screen name='Settings' component={SettingsStack}/>
			</Tabs.Navigator>
		)
	}
}

export default AppTabs;