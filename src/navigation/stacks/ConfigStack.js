import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Style } from '../../Style';
import { Text } from 'react-native'
import { TouchableOpacity } from 'react-native'
import ConfigurationScreen from '../../screens/Configuration/index';

const Stack = createStackNavigator();

export const ConfigStack = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen 
				name="Configuration" 
				options={{
					// headerRight: () => {
					// 	return (
					// 		<TouchableOpacity onPress={() => {
					// 			logout()
					// 		}}>
					// 			<Text>LOGOUT</Text>
					// 		</TouchableOpacity>
					// 	)
					}} 
				component={ConfigurationScreen} />
			<Stack.Screen 
				name="AddItem" 
				// options={{
				// 	headerRight: () => {
				// 		return (
				// 			<TouchableOpacity onPress={() => {
				// 				logout()
				// 			}}>
				// 				<Text>LOGOUT</Text>
				// 			</TouchableOpacity>
				// 		)
				// 	}}} 
				component={ConfigurationScreen} />
		</Stack.Navigator>
	);
}