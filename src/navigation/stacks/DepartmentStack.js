import React, { useContext } from 'react';
import { createStackNavigator, HeaderBackButton } from '@react-navigation/stack';
import { Button, View, TouchableOpacity } from 'react-native';
import CancelButton from '../../components/CancelButton';
import HeaderAddButton from '../../components/HeaderAddButton';

import InboundOverviewScreen from '../../screens/Department/Inbound/InboundOverview';
import ScannerScreen from '../../screens/ScannerScreen.js'
import ItemCreationScreen from '../../screens/Item/ItemCreationScreen';
import InboundReceivingScreen from '../../screens/Department/Inbound/InboundReceiving';
import ItemSearchScreen from '../../screens/Item/ItemSearchScreen';

const Stack = createStackNavigator();

export const DepartmentStack = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen 
				name="Inbound Overview" 
				component={InboundOverviewScreen}
				options={{
					headerRight: () => <View style={{marginRight: 12}}>
						<HeaderAddButton/>
					</View>
				}}	
			/>
			<Stack.Screen
				name="DepartmentScanner"
				options={{
					title: null,
					headerLeft: () => <View style={{marginLeft: 20}}>
						<CancelButton/>
					</View>,
				}}
				component={ScannerScreen}
			/>
			<Stack.Screen
				name="Inbound Receiving"
				component={InboundReceivingScreen}
				options={{
					headerLeft: () => <View style={{marginLeft: 20}}>
						<CancelButton/>
					</View>,
				}}
			/>
			<Stack.Screen
				name="Item Creation"
				component={ItemCreationScreen}
				options={{
					headerLeft: () => <View style={{marginLeft: 20}}>
						<CancelButton/>
					</View>,
					// headerRight: () => <View style={{marginRight: 12}}>
					// 	<SaveButton/>
					// </View>
				}}
			/>
			<Stack.Screen
				name="Item Search"
				options={{
					title: null,
				}}
				component={ItemSearchScreen}
			/>
		</Stack.Navigator>
	);
}