import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Style } from '../../Style';
import { Text } from 'react-native'
import { TouchableOpacity } from 'react-native'
import SettingsScreen from '../../screens/Settings/index';

const Stack = createStackNavigator();

export const SettingsStack = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen 
				name="Settings" 
				component={SettingsScreen} />
		</Stack.Navigator>
	);
}