import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Style } from '../../Style';
import { View } from 'react-native'
import HeaderAddButton from '../../components/HeaderAddButton';
import CancelButton  from '../../components/CancelButton';

import InventoryScreen from '../../screens/Inventory/index';
import ItemCreationScreen from '../../screens/Item/ItemCreationScreen';
import ScannerScreen from '../../screens/ScannerScreen.js'

const Stack = createStackNavigator();

class InventoryStack extends React.Component {
	render() {
		return (
			<Stack.Navigator>
				<Stack.Screen 
					name="Inventory" 
					component={InventoryScreen}
				 />
				<Stack.Screen
					name="Item Creation"
					component={ItemCreationScreen}
				/>
				<Stack.Screen
				name="InventoryScanner"
				options={{
					title: null,
					headerLeft: () => <View style={{marginLeft: 20}}>
						<CancelButton/>
					</View>,
				}}
				component={ScannerScreen}
				/>
			</Stack.Navigator>
	)};	
}

export default InventoryStack;