import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Style } from '../../Style';
import { Text, Image, SafeAreaView } from 'react-native'
import HomeScreen from '../../screens/Home/index';
import OutboundScreen from '../../screens/Department/Outbound/Outbound';
import OrderDetailScreen from '../../screens/Orders/components/Detail';

const Stack = createStackNavigator();

class HomeStack extends React.Component {
	render() {
		return (
			<Stack.Navigator>
				<Stack.Screen 
					name="Home" 
					options={{
						title: (
							<SafeAreaView style={{ alignItems: 'center', justifyContent: 'center', height: '80%', flexDirection: 'row'}}>
								<Image source={require('../../../assets/images/logos/fulfilled-empty-background.png')} />
							</SafeAreaView>),
						// headerRight: () => <FilterButton/>
						}} 
					component={HomeScreen}
					updateAuth={() => this.props.updateAuth('AppTabs')}
				 />
				<Stack.Screen
					name="Outbound"
					component={OutboundScreen}
				/>
				<Stack.Screen
					name="Order Detail"
					component={OrderDetailScreen}
				/>
			</Stack.Navigator>
	)};	
}

export default HomeStack;