import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Style } from '../../Style';
import { Text } from 'react-native'
import { TouchableOpacity } from 'react-native'
import OrdersScreen from '../../screens/Orders/index';

const Stack = createStackNavigator();

export const SettingsStack = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen 
				name="OrdersScreen" 
				component={OrdersScreen} />
		</Stack.Navigator>
	);
}