import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Style } from '../../Style';
import { Button } from 'react-native-elements';

import {
	View, 
	Text, 
	StyleSheet, 
	Image, 
	Dimensions, 
	KeyboardAvoidingView, 
	Platform
} from 'react-native';


import SignIn from '../../screens/Authentication/SignIn';
import SignUp from '../../screens/Authentication/SignUp';
import ForgetPassword from '../../screens/Authentication/ForgetPassword';

const Stack = createStackNavigator()

export class AuthStack extends React.Component {
	state = {
		showSignUp: false,
		formType: 'showSignIn'
	}
	toggleAuthType = formType => {
		this.setState({ formType })
	}

	render() {
		const showSignIn = this.state.formType === 'showSignIn';
		const showSignUp = this.state.formType === 'showSignUp';
		const showForgotPassword = this.state.formType === 'showForgotPassword';

		return (
			<KeyboardAvoidingView
			style={styles.container}
			behavior={Platform.Os == "ios" ? "padding" : "height"}
			>
				<Image
					style={styles.logo}
					resizeMode='contain'
					source={require("../../../assets/images/logos/fulfilled-white-background.png")}
				/>
				{ showSignIn && (
				<SignIn
					toggleAuthType={this.toggleAuthType}
					updateAuth={() => this.props.updateAuth('appTabs')}
				/>
				) }
				{ showSignUp && <SignUp toggleAuthType={this.toggleAuthType} /> }
				{ showForgotPassword && <ForgotPassword toggleAuthType={this.toggleAuthType} /> }
				<View style={{ position: 'absolute', bottom: 40 }}>
				{
				showSignUp || showForgotPassword ? (
					<Text style={styles.bottomMessage}>Already signed up? <Text
						style={styles.bottomMessageHighlight}
						onPress={() => this.toggleAuthType('showSignIn')}>&nbsp;&nbsp;Sign In</Text></Text>
				) : (
					<Text style={styles.bottomMessage}>Need an account?
					<Text
						onPress={() => this.toggleAuthType('showSignUp')}
						style={styles.bottomMessageHighlight}>&nbsp;&nbsp;Sign Up</Text>
					</Text>
				)
				}
				</View>
			</KeyboardAvoidingView>
		);
	}
}


const styles = StyleSheet.create({
	container: {
	  flex: 1,
	  // justifyContent: 'center',
	  alignItems: 'center',
	  paddingBottom: 40
	},  
	logo: {
		height: 350,
		padding: '10%',
		borderWidth: 5,
		borderColor: '#0F3F57'
	},
	title: {
	  fontSize: 26,
	  marginTop: 15,
	  // fontFamily: 'SourceSansPro-SemiBold',
	  color: '#ECAE2F'
	},
	subtitle: {
	  fontSize: 20,
	  marginBottom: 20,
	  color: '#0F3F57',
	//   fontFamily: 'SourceSansPro-Regular',
	},
	bottomMessage: {
	//   fontFamily: 'SourceSansPro-Regular',
	  fontSize: 18
	},
	bottomMessageHighlight: {
	  color: '#ECAE2F',
	  paddingLeft: 10
	}
   })
   
   export default AuthStack;