import React, { useState } from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { View, Text, ActivityIndicator } from 'react-native';
import { Style } from '../Style';
import AppTabs from './AppTabs';
import { AuthStack } from './stacks/AuthStack';
import { useNavigation } from '@react-navigation/native';

import { Auth as AmplifyAuth } from 'aws-amplify';

const AppTheme = {
	...DefaultTheme,
	colors: 
		{
			...DefaultTheme.colors,
			backgroundColor: '#F5FCFF'
		},
}

class Routes extends React.Component {

	state = {
		currentView: 'auth',
		loading: 'false'
	}

	// componentDidMount() {
	// 	this.checkAuth()
	// };


	// updateAuth = (currentView) => {
	// 	this.setState({ currentView })
	// }

	// checkAuth = async () => {
	// 	try {
	// 		this.setState({ loading: true })
	// 		await AmplifyAuth.currentAuthenticatedUser();
	// 		this.setState({ loading:false })
	// 		console.log('user is signed in')
	// 		this.setState({ currentView: 'appTabs'})
			
	// 	} catch (err) {
	// 		console.log('user is not signed in')
	// 		this.setState({ currentView: 'auth'})
	// 	}
	// }
	
	// if (loading) {
	// 	return <Style>
	// 		<ActivityIndicator size="large"/>
	// 	</Style>
	// }

	render() {
		return (
			<NavigationContainer>	
				{/* { currentView === 'auth' && <AuthStack updateAuth={this.updateAuth} /> } */}
				<AppTabs/>
			</NavigationContainer>
			);
	}
}

export default Routes;
