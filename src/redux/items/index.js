import { createReducer } from '../reducerCreator';

export const ADD_ITEM = 'ADD_ITEM';
export const DELETE_ITEM = 'DELETE_ITEM';
export const SET_CURRENT_ITEM = 'SET_CURRENT_ITEM';

export const ADJUST_ITEM_QUANTITY = 'ADJUST_ITEM_QUANTITY';
export const RECEIVE_INTO_INVENTORY = 'RECEIVE_INTO_INVENTORY';

export const SET_QUANTITY_ADJUSTMENT = 'SET_QUANTITY_ADJUSTMENT';


const defaultState = {
	currentItem: '',
	items: [],
	itemQuantities: [],
	quantityAdjust: false,
}

const itemSearch = (sku, itemQuantities) => {
	for (var i=0; i < itemQuantities.length; i++) {
		if (itemQuantities[i].sku === sku) {
			return itemQuantities[i];
		}
	}
}

const itemsReducer = createReducer(defaultState, {
	[ADD_ITEM](state, action) {
		let itemArray = state.items;
		itemArray.push(action.item);
		return {
			...state,
			items: itemArray
		}
	},
	[SET_CURRENT_ITEM](state, action) {
		let currentItem = action.item;
		return {
			...state,
			currentItem: currentItem
		}
	},
	[ADJUST_ITEM_QUANTITY](state, action) {
		let itemQuantities = state.itemQuantities;
		let selectedItem = itemSearch(action.sku, itemQuantities);
		selectedItem.quantity = action.quantity;
		selectedItem.date = action.date;

		let updatedList = itemQuantities.filter( item => item.sku !== action.sku);
		updatedList.push(selectedItem);

		return {
			...state,
			itemQuantities: updatedList
		}
	},
	[RECEIVE_INTO_INVENTORY](state, action) {
		let itemQuantities = state.itemQuantities;
		let filteredList;
		let newItem;


		if (itemQuantities.some(item => item.sku === action.inventoryItem.sku)){
			newItem = false;
			let existingItem = itemSearch(action.inventoryItem.sku, itemQuantities);
			existingItem.quantity = parseInt(existingItem.quantity) + parseInt(action.itemsReceived);
			
			filteredList = itemQuantities.filter(item => item.sku !== action.inventoryItem.sku);
			filteredList.push(existingItem);
		} else {
			newItem = action.inventoryItem;
			newItem.quantity = action.itemsReceived
			itemQuantities.push(newItem);
		}

		return {
			...state,
			itemQuantities: newItem ? itemQuantities : filteredList
		}
	},
	[SET_QUANTITY_ADJUSTMENT](state, action) {
		return{
			...state,
			quantityAdjust: action.quantityAdjust
		}
	}
})


export function additem(item) {
	return {
		type: ADD_ITEM,
		id: item.sku,
		item
	}
}


export function setcurrentitem(item) {
	return {
		type: SET_CURRENT_ITEM,
		item
	}
}


export function deleteitem(id) {
	return {
		type: DELETE_ITEM,
		payload: id
	}
}

export function adjustitemquantity(sku, quantity, date) {
	return {
		type: ADJUST_ITEM_QUANTITY,
		sku: sku,
		quantity: quantity,
		date: date
	}
}

export function receiveintoinventory(inventoryItem, itemsReceived){	
	return {
		type: RECEIVE_INTO_INVENTORY,
		inventoryItem: inventoryItem,
		itemsReceived: itemsReceived
	}
}

export function setquantityadjustment(quantityAdjust) {
	return {
		type: SET_QUANTITY_ADJUSTMENT,
		quantityAdjust: quantityAdjust
	}
}

export const namespace = 'items';
export const local = state => state[namespace];
export default itemsReducer;