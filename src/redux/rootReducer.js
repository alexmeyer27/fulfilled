import { combineReducers } from 'redux';

import inboundReducer, {
	namespace as inboundNamespace,
	defaultState as inboundDefaultState
} from './inbound/index';

import itemsReducer, {
	namespace as itemsNamespace,
	defaultState as itemsDefaultState
} from './items/index';

export default function createRootReducer(history, asyncReducers) {
	const combineReducer = combineReducers({
		[inboundNamespace]: inboundReducer,
		[itemsNamespace]:  itemsReducer
	})

	const rootReducer = (state, action) => {
		if (action.type === 'SIGN_OUT') {
		const { inbound } = state;
		state = {
			[inboundNamespace] : {
			...inboundDefaultState
		}
		}}
		return combineReducer(state, action)
	}
	return rootReducer;
}