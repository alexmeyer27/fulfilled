import { createStore } from 'redux'
import { persistStore, persistReducer, createTransform } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage';
 
import inboundReducer from './inbound/index';
import { defaultState as inboundDefaultState } from './inbound/index';
import { defaultState as itemsState } from './items/index';
import createRootReducer from './rootReducer';

const setInboundTransform = createTransform(
	(inboundState, key) => ({
		...inboundState,
		inboundSession: inboundState.inboundSession,
		currentInboundReceipt: inboundState.currentInboundReceipt,
		itemReceived: inboundState.itemReceived
	}),
	(outboundState, key) => ({
		...outboundState,
		inboundSession: false,
		currentInboundReceipt: {},
		itemReceived: false
	}),
	{ whitelist: ['inbound']}
);

const setItemsTransform = createTransform(
	(inboundState, key) => ({
		...inboundState,
		currentItem: inboundState.currentItem,
		quantityAdjust: inboundState.quantityAdjust,
	}),
	(outboundState, key) => ({
		...outboundState,
		currentItem: '',
		quantityAdjust: false
	})
)


 
const persistConfig = {
	key: 'root',
	storage: AsyncStorage,
	whitelist: ['inbound', 'items'],
	transforms: [ setItemsTransform, setInboundTransform ]
}

const rootReducer = createRootReducer();
 
const persistedReducer = persistReducer(persistConfig, rootReducer)
 
export default () => {
	let store = createStore(persistedReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
	let persistor = persistStore(store)
	return { store, persistor }
}