import remove from 'lodash';
import { createReducer } from '../reducerCreator';

export const ADD_INBOUND_RECEIPT = 'ADD_ITEM_RECEIPT';
export const EDIT_INBOUND_RECEIPT = 'EDIT_INBOUND_RECEIPT';
export const SET_INBOUND_SESSION = 'SET_INBOUND_SESSION';
export const SET_CURRENT_INBOUND_RECEIPT = 'SET_CURRENT_INBOUND_RECEIPT';

export const SET_ITEM_RECEIVING = 'SET_ITEM_RECEIVING';



const defaultState = {
	items: [],
	currentInboundReceipt: {},
	inboundReceipts: [],
	inboundSession: false,
	itemReceived: false
}

const inboundReceiptSearch = (currentInboundReceipt, inboundReceiptArray) => {
	for (var i=0; i < inboundReceiptArray.length; i++) {
		if (inboundReceiptArray[i] === currentInboundReceipt) {
			return inboundReceiptArray[i];
		}
	}
}

const inboundReducer = createReducer(defaultState, {
	[ADD_INBOUND_RECEIPT](state, action) {
		let newInboundReceipts = state.inboundReceipts;
		newInboundReceipts.unshift(action.inboundReceipt)
		
		return {
			...state,
			inboundReceipts: newInboundReceipts
		}
	},
	[EDIT_INBOUND_RECEIPT](state, action) {
		let newInboundReceipt = action.inboundReceipt;
		let inboundReceiptList = state.inboundReceipts;
		let editedReceipt = inboundReceiptSearch(state.currentInboundReceipt, inboundReceiptList);
		console.log('EDITED RECEIPT: ', editedReceipt);
		newInboundReceipt.date = editedReceipt.date;

		let filteredList = inboundReceiptList.filter(receipt => receipt.date !== editedReceipt.date);
		filteredList.push(action.inboundReceipt);
		filteredList.sort((a, b) => (a.date > b.date) ? -1 : 1)


		return {
			...state,
			inboundReceipts: filteredList
		}
	},
	[SET_INBOUND_SESSION](state, action) {
		return {
			...state,
			inboundSession: action.payload
		}
	},
	[SET_CURRENT_INBOUND_RECEIPT](state, action) {
		return {
			...state,
			currentInboundReceipt: action.payload
		}
	},
	[SET_ITEM_RECEIVING](state, action) {
		return {
			...state,
			itemReceived: action.payload
		}
	}
})

export function addinboundreceipt(inboundReceipt) {
	return {
		type: ADD_INBOUND_RECEIPT,
		inboundReceipt
	}
}

export function editinboundreceipt(inboundReceipt) {
	return {
		type: EDIT_INBOUND_RECEIPT,
		inboundReceipt
	}
}


export function setinboundsession(inboundSession) {
	return {
		type: SET_INBOUND_SESSION,
		payload: inboundSession
	}
}

export function setcurrentinboundreceipt(currentInboundReceipt) {
	return {
		type: SET_CURRENT_INBOUND_RECEIPT,
		payload: currentInboundReceipt
	}
}

export function setitemreceiving(itemReceived) {
	return {
		type: SET_ITEM_RECEIVING,
		payload: itemReceived
	}
}

export const namespace = 'inbound';
export const local = state => state[namespace];
export default inboundReducer;