import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import BarcodeScanner from '../../../components/BarcodeScanner';
import { Tooltip, Text, SearchBar, Card, Button, Divider, Overlay } from 'react-native-elements';
import { Entypo } from '@expo/vector-icons';

const ItemCard = (props) => {	
		return (
				<View>
					<Card title={items.currentItem.name} titleStyle={{fontSize: 20}} containerStyle={[styles.card, {maxHeight: '32%', flex: 1}]}>
						<Entypo name="dot-single" color="green" style={{justifyContent: 'center', alignItems: 'center'}} size={75}/>
						<View style={{flexDirection: 'row'}}>
							<View style={{ marginBottom: 5, width: '50%'}}>
								<Text style={styles.headers}>SKU:</Text>
								<Text style={styles.headers}>Quantity:</Text>
								<Text style={styles.headers}>Last Received:</Text>
							</View>
							<View style={{ marginBottom: 5, alignItems: "flex-end", marginRight: 5, width: '50%'}}>
								<Text style={styles.values}>{items.currentItem.sku}</Text>
								<Text style={styles.values}>{items.currentItem ? itemSearch(items.currentItem.sku, items.itemQuantities).quantity : ''}</Text>
								<Text style={styles.values}>{items.currentItem ? itemSearch(items.currentItem.sku, items.itemQuantities).date ? itemSearch(items.currentItem.sku, items.itemQuantities).date : 'N/A' : ''}</Text>
							</View>
						</View>
					</Card>
				</View>
		)
	}

const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	},
	buttonContainer: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		flex: 1
	},
	container: {
		flex: 1,
		backgroundColor: '#F5FCFF',
		// borderWidth: 1,
		flexDirection: 'column',
	},
	buttonContainerStyle: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		padding: 10,
		margin: 20,
		height: 60

	},
	buttonText: {
		color: 'white',
		fontSize: 16,
		padding: 4
	},
	searchBar: {
		backgroundColor: '#fcf3db',
		borderRadius: 30,
		fontSize: 16,
		paddingHorizontal: 14,
		color: 'black'
	}
})

export default ItemCard;
