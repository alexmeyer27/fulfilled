import React, { Component, useState, useEffect } from 'react';
import { 
	Text, 
	StyleSheet, 
	TouchableOpacity, 
	View, 
	ScrollView,
	FlatList,
	Image,
	Touchable,
	KeyboardAvoidingView
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { additem, deleteitem, setinboundsession} from '../../redux/inbound/index';
import { Card, Divider, Overlay, Input } from 'react-native-elements';
import ActionButton from '../../components/ActionButton';
import { setquantityadjustment, setcurrentitem, adjustitemquantity } from '../../redux/items';
import moment from 'moment';
import Entypo from 'react-native-vector-icons/Entypo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';


function InventoryScreen({ navigation }) {

	const items = useSelector(state  => state.items)
	const dispatch = useDispatch();
	const setCurrentItem = item => dispatch(setcurrentitem(item));
	const setInboundSession = inboundSession => dispatch(setinboundsession(inboundSession));
	const setQuantityAdjustment = quantityAdjustment => dispatch(setquantityadjustment(quantityAdjustment));
	const adjustItemQuantity = (sku, quantity, date) => dispatch(adjustitemquantity(sku, quantity, date));

	const itemSearch = (sku, itemsArray) => {
		for (var i=0; i < itemsArray.length; i++) {
			if (itemsArray[i].sku === sku) {
				return itemsArray[i];
			}
		}
	}

	let tableDataArray = [];
	const generateItemTableData = ( items ) => {
		let i;
		for (i = 0; i < items.itemQuantities.length; i++) {
			let itemObject = items.itemQuantities[i];
			let date;

			if (itemObject.date === '') {
				date = 'N/A'
			} else {
				date = itemObject.date;
			}
			let itemArray = [ itemObject.name, itemObject.sku, itemObject.quantity, date]
			
			tableDataArray.push(itemArray)
		};
		return tableDataArray;
	}

	const [tableData, setTableData] = useState('');
	const [adjustOverlayVisible, setAdjustOverlayVisible] = useState(false);
	const [quantity, setQuantity] = useState('');
	const [confirmedQuantity, setConfirmedQuantity] = useState('');

	const displayDate = (date) => {
		return date.split(' ')[0];
	}

	const toggleAdjustOverlay = (visible) => {
		setAdjustOverlayVisible(visible);
		setQuantity('');
		setConfirmedQuantity('');
	};

	React.useLayoutEffect(() => {
		navigation.setOptions({
		  headerRight: () => (
			<TouchableOpacity onPress={() => {
				navigation.navigate('Item Creation', { scannedUpc: '', receiving: false });
			}}>
				<Text style={{color: '#0F3F57', fontWeight: '800', marginRight: 10}}>
				Add Item
				</Text>
			</TouchableOpacity>
		  ),
		});
	  }, [navigation]);

	useEffect(() => {		
		setTableData(generateItemTableData(items));
		if (items.quantityAdjust === true) {
			toggleAdjustOverlay(true)
		}
	}, [items]);

		return (
			<KeyboardAwareScrollView style={styles.container} >
				<Overlay isVisible={adjustOverlayVisible} onBackdropPress={() => { 
						toggleAdjustOverlay(false);
						setQuantityAdjustment(false); 
					}}>
						<KeyboardAvoidingView>
						<Card titleStyle={{fontSize: 20}} containerStyle={[styles.card, {maxHeight: '35%', flex: 1}]}>
						<View style={{flexDirection: 'row', marginTop: 15}}>
							<Image style={{width: 80, height: 80, marginRight: 15, marginTop: 10 }} source={require('../../../assets/images/sample-png.png')}/>
								<View style={{ marginBottom: 5, width: '32%'}}>
									<Text style={styles.headers}>SKU:</Text>
									<Text style={styles.headers}>Quantity:</Text>
									<Text style={styles.headers}>Last Received:</Text>
								</View>
								<View style={{ marginBottom: 5, alignItems: "flex-end", width: '40%'}}>
									<Text style={styles.values}>{items.currentItem.sku}</Text>
									<Text style={styles.values}>{items.currentItem ? itemSearch(items.currentItem.sku, items.itemQuantities).quantity : ''}</Text>
									<Text style={styles.values}>{items.currentItem ? itemSearch(items.currentItem.sku, items.itemQuantities).date ? itemSearch(items.currentItem.sku, items.itemQuantities).date : 'N/A' : ''}</Text>
								</View>
							</View>
						</Card>
						<View style={{marginTop: 20}}>
							<Input 
								label={"Quantity"} 
								onChangeText={value => setQuantity(value)}
								value={items.currentItem ? quantity : ''}
								keyboardType={'number-pad'}
								labelStyle={styles.label}		
							/>
							<Input 
								label={"Confirm Quantity"} 
								onChangeText={value => setConfirmedQuantity(value)}
								value={items.currentItem ? confirmedQuantity : ''}
								keyboardType={'number-pad'}
								labelStyle={styles.label}		
							/>
						</View>
						<View style={{marginTop: 20}}>
							<ActionButton title="Submit" onPress={() => {
								let date = moment().format('M/D/YYYY');
								if (quantity && confirmedQuantity && confirmedQuantity !== quantity) {
									alert('Quantities need to match to adjust quantity.')
								} else {
									toggleAdjustOverlay(false);
									adjustItemQuantity(items.currentItem.sku, quantity, date);
									setQuantityAdjustment(false)
								}
							}} />
						</View>
						</KeyboardAvoidingView>
				</Overlay>
				<View style={{padding: 20, paddingBottom: 1}}>
						<ActionButton title="Adjust Item Quantity" onPress={() => {
							setInboundSession(false);
							navigation.navigate('InventoryScanner');
						}} />
					</View>
					<Card title="Current Inventory" titleStyle={{fontSize: 18}} containerStyle={[styles.card, { marginBottom: 10}]}>
						<ScrollView style={{marginTop: 10}}>
						<FlatList
							data={items.itemQuantities}
							renderItem={({item}) => {
									return (
										<TouchableOpacity 
											style={{marginLeft: '2.5%',  marginRight: '2.5%', backgroundColor: item.quantity < 10 ? '#FF6961' : '#90EE90', borderWidth: 1, marginBottom: 5 }} 
											onPress={() => {
												setCurrentItem(item);
												toggleAdjustOverlay(true);
											}}
										>
											<View style={{flexDirection: 'row', marginBottom: 10}}>
												<Image style={{ marginLeft: 15, marginRight: 20, width: 100, height: 100, marginTop: 15 }} source={require('../../../assets/images/sample-png.png')}/>
												<View style={{width: '25%'}}>
													<Text style={styles.headers}>Name:</Text>
													<Text style={styles.headers}>SKU:</Text>
													<Text style={styles.headers}>Units:</Text>
													<Text style={styles.headers}>Date:</Text>
												</View>
												<View style={{ marginRight: 10, alignItems: "flex-end", width: '33%'}}>
													<Text style={styles.values}>{item.name}</Text>
													<Text style={styles.values}>{item.sku}</Text>
													<Text style={styles.values}>{item.quantity}</Text>
													<Text style={styles.values}>{item.date ? displayDate(item.date) : 'N/A'}</Text>
												</View>
											</View>
											{/* <Entypo name="dot-single" color="green" style={{position: 'absolute', marginLeft: '85%' }} size={65}/> */}
											<Divider/>
										</TouchableOpacity>
									)
								}}  
							keyExtractor={(item, index) => index}
							ListEmptyComponent={() => {
								return (
									<Text>No items in inventory!</Text>
								)
							}}
						/>
						</ScrollView>
					</Card>
			</KeyboardAwareScrollView>
	)}



const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		padding: 2
	},
	buttonContainer: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		flex: 1,
	},
	container: {
		flex: 1,
		backgroundColor: '#F5FCFF',
		flexDirection: 'column',
	},
	card: {
		marginLeft: 5,
		marginRight: 5 
	},
	values: {
		fontSize: 15,
		marginTop: 10
	},
	headers: {
		fontSize: 15,
		fontWeight: '600',
		marginTop: 10
	},
})

export default InventoryScreen;