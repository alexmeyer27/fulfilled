import React, { Component, useState, useEffect } from 'react';
import {
	StyleSheet,
	Text,
	TouchableOpacity,
	View
} from 'react-native';
import { useNavigation, useRoute, useIsFocused } from '@react-navigation/native';
import { connect, useSelector, useDispatch } from 'react-redux';
import { Tooltip, SearchBar, Card, Button, Divider, Overlay } from 'react-native-elements';

import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { setcurrentitem, setquantityadjustment } from '../redux/items/index';

export default function ScannerScreen() {
	const isFocused = useIsFocused()
	
	const navigation = useNavigation();

	const dispatch = useDispatch();
	const inbound = useSelector(state => state.inbound)
	const items = useSelector(state => state.items)
	const setCurrentItem = item => dispatch(setcurrentitem(item));
	const setQuantityAdjustment = item => dispatch(setquantityadjustment(item));

	const [scannerActivated, setScannerActivated] = useState(false)

	const itemsSearch = (upc, itemsArray) => {
		for (var i=0; i < itemsArray.length; i++) {
			if (itemsArray[i].upc === upc) {
				return itemsArray[i];
			}
		}
	}

	const routeName = useRoute().name;

	useEffect(() => {		
		setScannerActivated(true);
		console.log(scannerActivated)
	}, [isFocused]);

	return (
		<QRCodeScanner
			onRead={(e) => {
				let itemCreated = itemsSearch(e.data, items.items)
				let navigationScreen;
				let receiving;

				if (!itemCreated){
					return navigation.navigate('Item Creation', {
						scannedUpc: e,
						receiving: true,
						receivedItemCreate: true
					});
				}
				 

				if (routeName === 'InventoryScanner' && itemCreated) {
					navigationScreen = 'Inventory';
					setQuantityAdjustment(true);
					receiving = false;
				} else {
					navigationScreen = 'Inbound Receiving';
					receiving = true;
				}
				
				setCurrentItem(itemCreated);
				navigation.navigate(navigationScreen, {
					scannedUpc: e,
					receiving: receiving,
					receivedItemCreate: false
				});
			}}
			flashMode={RNCamera.Constants.FlashMode.off}
			topContent={
				<View>
					<Text style={{fontSize: 20}}>
						Scan your item's barcode.
					</Text>
				</View>
			}
			reactivate={true}
			reactivateTimeout={5000}
			// bottomContent={
			// 	<TouchableOpacity onPress={() => {
			// 		navigation.navigate('Item Creation', {
			// 			upc: '12345',
			// 			receiving: true
			// 		})
			// 	}}>
			// 		{/* <Text>Test Item Create</Text> */}
			// 		<SearchBar
			// 			placeholder="Search by item code..."
			// 			onChangeText={() => console.log('')}
			// 			value={'test'}
			// 			// inputStyle={styles.searchBar}
			// 			// containerStyle={styles.buttonContainerStyle}
			// 			// inputContainerStyle={styles.buttonContainer}
			// 		/>	
			// 	</TouchableOpacity>
			// }
		/>
	)
}
