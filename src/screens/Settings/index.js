import React, { Component } from 'react';
import { Dimensions, StyleSheet, SafeAreaView } from 'react-native';
import moment from 'moment';
import { useNavigation, StackActions } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome5'
import { ListItem } from 'react-native-elements';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { Auth } from 'aws-amplify';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { TouchableOpacity } from 'react-native-gesture-handler';



class Settings extends React.Component {
	updateAuth = (currentView) => {
		this.setState({ currentView })
		console.log(currentView)
	}
	
	signOut = async () => {
		try {
			await Auth.signOut()
			console.log('signed out')
			this.props.navigation.navigate('AuthStack', {screen: 'SignIn'});
		} catch (err) {
			console.log('error signing out: ', err)
		}
	}

	render () {
		const { navigation } = this.props;

		return (
			<SafeAreaView>
				<TouchableOpacity>
					<ListItem
						title="Account"
						leftIcon={<MaterialIcons name={'account-circle'} size={22} color={'black'} style={{marginRight: 10}} />}
					/>
				</TouchableOpacity>
				<TouchableOpacity
					onPress={this.signOut}
				>
					<ListItem
						title="Logout"
						leftIcon={<MaterialCommunityIcons name={'logout'} size={22} color={'black'} style={{marginRight: 10}} />}
					/>
				</TouchableOpacity>
				<TouchableOpacity>
					<ListItem
						title="Connect"
						leftIcon={<MaterialIcons name={'import-export'} size={22} color={'black'} style={{marginRight: 10}} />}
					/>
				</TouchableOpacity>
			</SafeAreaView>
		)
	}
}

const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	}
})

export default function (props){
	const navigation = useNavigation();

	return <Settings {...props} navigation={navigation}/>
};