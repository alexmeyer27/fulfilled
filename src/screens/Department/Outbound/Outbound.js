import React, { Component } from 'react';
import { Text, Dimensions, StyleSheet, View, Image, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native'


export default function OutboundScreen() {
	const navigation = useNavigation();

	return (
		<View>
			<Text style={styles.text}>Outbound Test Screen</Text>
		</View>
	)
}

const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	}
})
