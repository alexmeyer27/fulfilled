import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import BarcodeScanner from '../../../components/BarcodeScanner';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import Input from '../../../components/Input';
import ActionButton from '../../../components/ActionButton';
import { Tooltip, Text, SearchBar, Card, Button, Divider, Overlay } from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';
import Feather from 'react-native-vector-icons/Feather';


const InboundOverlay = (props) => {	
	// state = {
	// 	search: '',
	// }
	
	// searchItems = (search) => {
	// 	this.setState({ search });
	// };
	
	// render() {
	// 	const { search } = this.state;
	// 	// const { navigation } = this.props;

		return (
			<Overlay isVisible={props.inboundShipmentVisibile} onBackdropPress={props.toggleInboundShipment}>
				{/* <Card title="Last Received" titleStyle={{fontSize: 14}} containerStyle={[styles.card, {maxHeight: '30%'}]}>
						<TouchableOpacity style={{position: 'absolute', marginLeft: '95%' }} onPress={() => {navigation.navigate('Item Creation')}}>
							<Feather name="edit" size={18} color="blue" />
						</TouchableOpacity>
						<View style={{flexDirection: 'row', marginBottom: 5}}>
							<Text>Date Received:</Text>
							<TouchableOpacity>
								<Text style={{color: '#0000EE', marginLeft: 198}}>12/31/20</Text>
							</TouchableOpacity>
						</View>
						<View style={{flexDirection: 'row', marginBottom: 5}}>
							<Text>Item Number:</Text>
							<TouchableOpacity>
								<Text style={{color: '#0000EE', marginLeft: 220}}>12345</Text>
							</TouchableOpacity>
						</View>
						<View style={{flexDirection:'row'}}>
							<Text style={{marginRight: 220}}>Order Quantity: </Text>
							<Text>20</Text>
						</View>
					</Card> */}
				
				<View>
					<View style={{marginTop: 20, marginBottom: 20}}>
						<Card title={null} style={{flexDirection: "row"}}>
							<Text style={{textAlign: 'center'}}>Scan or search your item's barcode to begin!</Text>
							{/* <Tooltip popover={<Text>Sample description</Text>}>
								<MaterialCommunityIcons name="information-outline" size={20}/>
							</Tooltip> */}
						</Card>
					</View>
					<View style={{flexDirection: "row", justifyContent: "center", margin: 25}}>
						<BarcodeScanner/>
					</View>
					<Divider style={{margin: 50, marginTop: 30}}/>
					<View style={{marginRight: 10, marginLeft: 10}}>
						<SearchBar
							placeholder="Search by item code..."
							onChangeText={() => {}}
							value={'search'}
							inputStyle={styles.searchBar}
							// containerStyle={styles.buttonContainerStyle}
							// inputContainerStyle={styles.buttonContainer}
						/>		
					</View>
				</View>
			</Overlay>
		)
	}

const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	},
	buttonContainer: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		flex: 1
	},
	container: {
		flex: 1,
		backgroundColor: '#F5FCFF',
		// borderWidth: 1,
		flexDirection: 'column',
	},
	buttonContainerStyle: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		padding: 10,
		margin: 20,
		height: 60

	},
	buttonText: {
		color: 'white',
		fontSize: 16,
		padding: 4
	},
	searchBar: {
		backgroundColor: '#fcf3db',
		borderRadius: 30,
		fontSize: 16,
		paddingHorizontal: 14,
		color: 'black'
	}
})

export default InboundOverlay;

// export default function(props) {
// 	const navigation = useNavigation();

// 	return <InboundOverlay {...props} navigation={navigation} />
// }