import React, { Component, useState } from 'react';
import { 
	Text, 
	StyleSheet, 
	TouchableOpacity, 
	View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { additem, deleteitem, setinboundsession, setcurrentinboundreceipt, setitemreceiving} from '../../../redux/inbound/index';
import { setcurrentitem } from '../../../redux/items/index';
import { Card, Divider, Tooltip, Overlay, SearchBar, CheckBox } from 'react-native-elements';
import ActionButton from '../../../components/ActionButton';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { get } from 'lodash';
import { SwipeListView } from 'react-native-swipe-list-view';



function InboundOverviewScreen({ navigation }) {

	const [inboundShipmentVisible, setInboundShipmentVisible] = useState(false);
	const [itemReceiveVisible, setItemReceiveVisible] = useState(false);
	const [inboundSessionEnabled, setInboundSessionEnabled] = useState(false);

	const items = useSelector(state  => state.items)
	const inbound = useSelector(state => state.inbound)
	const dispatch = useDispatch();
	const setCurrentItem = item => dispatch(setcurrentitem(item));
	const setCurrentInboundReceipt = receipt => dispatch(setcurrentinboundreceipt(receipt));
	const setInboundSession = inboundSession => dispatch(setinboundsession(inboundSession));
	const setItemReceiving = itemReceived => dispatch(setitemreceiving(itemReceived));

	const toggleInboundShipmentModal = () => {
		setInboundShipmentVisible(!inboundShipmentVisible)
	};

	const toggleItemReceiveModal = () => {
		setItemReceiveVisible(!itemReceiveVisible)
	};
	
	const toggleInboundSessionEnabled = () => {
		setInboundSessionEnabled(!inboundSessionEnabled)
	};

	const itemSearch = (sku, itemsArray) => {
		for (var i=0; i < itemsArray.length; i++) {
			if (itemsArray[i].sku === sku) {
				return itemsArray[i];
			}
		}
	}

		return (
			<View style={styles.container} >
				<Overlay isVisible={inboundShipmentVisible} onBackdropPress={() => { toggleInboundShipmentModal() }}>
						<View style={{padding: 20, paddingBottom: 1}}>
								<ActionButton title="Scan Barcode" onPress={() => {
									toggleInboundShipmentModal()
									navigation.navigate('DepartmentScanner')
								}} />

						</View>
						<View style={{padding: 20}}>
							<ActionButton title="Search Item by UPC" onPress={() => {
								toggleInboundShipmentModal()
								navigation.navigate('Item Search')
							}} />
						</View>
				</Overlay>
				{/* <Overlay isVisible={itemReceiveVisible} onBackdropPress={() => {toggleItemReceive()}}>
					<SafeAreaView>
						<View style={{padding: 20, paddingBottom: 1}}>
							<Text>Item Quantity</Text>
						</View>
					</SafeAreaView>
				</Overlay> */}
				<View style={{height: '65%'}}>
					<Card title="Last Received" titleStyle={{fontSize: 14}} containerStyle={[styles.card, {maxHeight: '30%'}]}>
					{(inbound.inboundReceipts.length !== 0) ?
						(<TouchableOpacity style={{position: 'absolute', marginLeft: '95%' }} onPress={() => {
								setCurrentItem(itemSearch(inbound.inboundReceipts[0].sku, items.items));
								setCurrentInboundReceipt(inbound.inboundReceipts[0]);
								setItemReceiving(true);
								navigation.navigate('Inbound Receiving', {scannedUpc: '', itemQuantity: inbound.inboundReceipts[0].quantity})
							}}>
							 <Feather name="edit" size={18} color="blue" />
						</TouchableOpacity>) : ( null )}
						{(inbound.inboundReceipts.length !== 0) ? 
						<View style={{flexDirection: 'row'}}>
							<View style={{width: '50%'}}>
								<Text style={styles.headers}>Date Received:</Text>
								<Text style={styles.headers}>Item:</Text>
								<Text style={styles.headers}>SKU:</Text>
								<Text style={styles.headers}>Quantity:</Text>
							</View>
							<View style={{alignItems: "flex-end", width: '50%'}}>
								<Text style={styles.values}>{inbound.inboundReceipts[0].date}</Text>
								<Text style={styles.values}>{inbound.inboundReceipts[0].sku}</Text>
								<Text style={styles.values}>{inbound.inboundReceipts[0].name}</Text>
								<Text style={styles.values}>{`${inbound.inboundReceipts[0].quantity}`}</Text>
							</View>
						</View> : <Text style={{marginLeft: 10}}>No recent shipments...</Text>
						}
					</Card>
					<Card title="Recent Inbound Shipments" titleStyle={{fontSize: 14}} containerStyle={[styles.card, { height: '80%'}]}>
						{/* <ScrollView style={{ marginBottom: 65}}> */}
							<SwipeListView
								style={{height: '85%'}}
								data={inbound.inboundReceipts}
								ListEmptyComponent={<Text style={{marginLeft: 10}}>No recent shipments...</Text>}
								keyExtractor={receipt => String(receipt.sku)}
								renderItem={(data, rowMap) => (
									<View>
										<View style={{flexDirection: 'row'}}>
											<View style={{width: '50%'}}>
												<Text style={styles.headers}>Date Received:</Text>
												<Text style={styles.headers}>Item:</Text>
												<Text style={styles.headers}>SKU:</Text>
												<Text style={styles.headers}>Quantity:</Text>
											</View>
											<View style={{alignItems: "flex-end", width: '50%'}}>
												<Text style={styles.values}>{data.item.date}</Text>
												<Text style={styles.values}>{data.item.sku}</Text>
												<Text style={styles.values}>{data.item.name}</Text>
												<Text style={styles.values}>{data.item.quantity}</Text>
											</View>		
										</View>								
										<Divider style={{marginTop: 5, marginBottom: 5}}/>
									</View>
								)}
								renderHiddenItem={ (data, rowMap) => (
									<View>
										{/* <TouchableOpacity onPress={() => {navigation.navigate('Item Creation', {scannedUpc: '', itemReceived: false})}}>
											<Feather name="edit" size={18} color="blue" />
										</TouchableOpacity> */}
									</View>
								)}
								rightOpenValue={-75}
							/>
						{/* </ScrollView> */}
					</Card>
					<Divider style={{marginRight: 50, marginLeft: 50, marginTop: 25}}/>
					<View style={{padding: 20, paddingBottom: 1}}>
						<ActionButton title="Receive Items" onPress={() => {
							setInboundSession(inboundSessionEnabled);
							navigation.navigate('DepartmentScanner');
						}} />
					</View>
					<TouchableOpacity style={{padding: 20}} onPress={() => {
						toggleInboundSessionEnabled()
					}}>
						<CheckBox 
							title='Enable Inbound Session'
							checked={inboundSessionEnabled}
							disabled={true}
						/>
					</TouchableOpacity>
				</View>
			</View>
	)}



const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	},
	buttonContainer: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		flex: 1,
	},
	container: {
		flex: 1,
		backgroundColor: '#F5FCFF',
		flexDirection: 'column',
	},
	card: {
		marginLeft: 20,
		marginRight: 20 
	}
})

export default InboundOverviewScreen;
