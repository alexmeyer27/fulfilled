import React, { Component, useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, SafeAreaView, KeyboardAvoidingView } from 'react-native';
import ActionButton from '../../../components/ActionButton';
import { Text, Card, Input } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

import { addinboundreceipt, editinboundreceipt, setitemreceiving } from '../../../redux/inbound/index';
import { receiveintoinventory } from '../../../redux/items/index';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';


function InboundReceivingScreen({ navigation, route }){	
	const { scannedUpc, itemQuantity } = route.params;

	const inbound = useSelector(state => state.inbound)
	const items = useSelector(state  => state.items);
	const dispatch = useDispatch();

	const [quantity, setQuantity] = useState(itemQuantity > 0 ? itemQuantity : '')
	const [confirmedQuantity, setConfirmedQuantity] = useState(itemQuantity > 0 ? itemQuantity : '')
	const [currentDate, setCurrentDate] = useState('');

	const addInboundReceipt = receipt => dispatch(addinboundreceipt(receipt));
	const editInboundReceipt = receipt => dispatch(editinboundreceipt(receipt));
	const receiveIntoInventory = (item, quantity) => dispatch(receiveintoinventory(item, quantity));
	const setItemReceiving = itemReceived => dispatch(setitemreceiving(itemReceived));

		return (
			<KeyboardAwareScrollView style={styles.container}>
				<Card title="Receiving Item" titleStyle={{fontSize: 20}} containerStyle={[styles.card, {maxHeight: '45%', flex: 1}]}>
					<View style={{flexDirection: 'row'}}>
						<View style={{ marginBottom: 5, width: '50%'}}>
							<Text style={styles.headers}>Item:</Text>
							<Text style={styles.headers}>SKU:</Text>
							<Text style={styles.headers}>Dimensions:</Text>
							<Text style={styles.headers}>Cost:</Text>
						</View>
						<View style={{ marginBottom: 5, alignItems: "flex-end", marginRight: 5, width: '50%'}}>
							<Text style={styles.values}>{items.currentItem.name}</Text>
							<Text style={styles.values}>{items.currentItem.sku}</Text>
							<Text style={styles.values}>{`${items.currentItem.length} X ${items.currentItem.width} X ${items.currentItem.height}`}</Text>
							<Text style={styles.values}>{`$${items.currentItem.cost}`}</Text>
						</View>
					</View>
				</Card>
				<Card>
					<View style={{marginTop: 20}}>
						<Input 
							label={"Quantity"} 
							onChangeText={value => setQuantity(value)}
							value={quantity ? quantity : 0}
							keyboardType={'number-pad'}
							labelStyle={styles.label}		
						/>
						<Input 
							label={"Confirm Quantity"} 
							onChangeText={value => setConfirmedQuantity(value)}
							value={confirmedQuantity ? confirmedQuantity : 0}
							keyboardType={'number-pad'}
							labelStyle={styles.label}		
						/>
					</View>
					<View style={{padding: 20}}>
						<ActionButton title="Receive Items" onPress={() => {
							let date = moment().format('M/D/YYYY hh:mm a');
							let inboundReceipt = { sku: items.currentItem.sku, name: items.currentItem.name, quantity: quantity };
							inbound.itemReceived ? null : inboundReceipt.date = date;

							if (confirmedQuantity !== quantity) {
								alert('Quantities need to match to submit receipt')
							} else {
								receiveIntoInventory({sku: items.currentItem.sku, name: items.currentItem.name, date: date}, quantity)
								inbound.itemReceived ? editInboundReceipt(inboundReceipt) : addInboundReceipt(inboundReceipt);
								(inbound.inboundSession === true && !inbound.itemReceived) ? navigation.navigate('DepartmentScanner') : navigation.navigate('Inbound Overview');
								setItemReceiving(false)
							}
						}} />
					</View>
				</Card>
			</KeyboardAwareScrollView>
		)
	}

const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	},
	buttonContainer: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		flex: 1
	},
	container: {
		flex: 1,
		backgroundColor: '#F5FCFF',
		// borderWidth: 1,
		flexDirection: 'column',
	},
	buttonContainerStyle: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		padding: 10,
		margin: 20,
		height: 60

	},
	buttonText: {
		color: 'white',
		fontSize: 16,
		padding: 4
	},
	searchBar: {
		backgroundColor: '#fcf3db',
		borderRadius: 30,
		fontSize: 16,
		paddingHorizontal: 14,
		color: 'black'
	},
	headers: {
		fontSize: 20,
		fontWeight: '600',
		marginTop: 10
	},
	label: {
		color: '#0F3F57'
	},
	values: {
		fontSize: 20,
		marginTop: 10
	}
})


export default function(props) {
	const navigation = useNavigation();

	return <InboundReceivingScreen {...props} navigation={navigation} />
}
