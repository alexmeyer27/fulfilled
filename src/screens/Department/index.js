import React, { Component } from 'react';
import { Text, Dimensions, StyleSheet, View, Image, Button } from 'react-native';
import moment from 'moment';
import { useNavigation } from '@react-navigation/native'

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Icon from 'react-native-vector-icons/FontAwesome5'

const { height, width } = Dimensions.get('window');

export default function DepartmentScreen() {
	const navigation = useNavigation();

	return (
		<View>
			<Image source={require('../../../assets/images/logos/fulfilled-white-background.png')} style={{ width: wp('75%'), height: hp('30%'), alignSelf: "center", marginTop: 20}}/>
			<Text style={styles.text}>Department Test Screen</Text>
			<Button
				title="Home"
				onPress={() => navigation.navigate('Home')}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	}
})
