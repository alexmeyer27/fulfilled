import React, { Component, useState } from 'react';
import { 
	Text, 
	Dimensions, 
	StyleSheet, 
	TouchableOpacity, 
	SafeAreaView, 
	View, 
	SectionList,
	Button,
	ScrollView,
	FlatList,
	Image } from 'react-native';
import { Card, ListItem, Divider, Tooltip } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import ActionButton from '../../components/ActionButton';
import Modal from 'react-native-modal';
import { SwipeListView } from 'react-native-swipe-list-view';
import { useSelector, useDispatch } from 'react-redux';
import Feather from 'react-native-vector-icons/Feather';
import { additem, deleteitem, setinboundsession, setitemreceiving, setcurrentinboundreceipt} from '../../redux/inbound/index';
import { setcurrentitem } from '../../redux/items/index';


function HomeScreen({navigation}) {

	const inbound = useSelector(state  => state.inbound);
	const items = useSelector(state  => state.items);
	const dispatch = useDispatch();
	const setCurrentItem = item => dispatch(setcurrentitem(item));
	const setItemReceiving = itemReceived => dispatch(setitemreceiving(itemReceived));
	const setCurrentInboundReceipt = receipt => dispatch(setcurrentinboundreceipt(receipt));

	const itemSearch = (sku, itemsArray) => {
		for (var i=0; i < itemsArray.length; i++) {
			if (itemsArray[i].sku === sku) {
				return itemsArray[i];
			}
		}
	};

	const displayDate = (date) => {
		return date.split(' ')[0];
	}

	return (
		<SafeAreaView style={{ flex: 1 }} >
			<View style={styles.container}>
				<View style={{marginTop: 10, maxHeight: '33%'}}>
				<Card title="Last Received" titleStyle={{fontSize: 14}}>
					{(inbound.inboundReceipts.length !== 0) ?
						(<TouchableOpacity style={{position: 'absolute', marginLeft: '95%' }} onPress={() => {
								setCurrentItem(itemSearch(inbound.inboundReceipts[0].sku, items.items));
								setCurrentInboundReceipt(inbound.inboundReceipts[0]);
								setItemReceiving(true);
								navigation.navigate('Department', { screen: 'Inbound Receiving', params: {scannedUpc: '', itemQuantity: inbound.inboundReceipts[0].quantity} })
							}}>
							 <Feather name="edit" size={18} color="blue" />
						</TouchableOpacity>) : ( null )}
						{(inbound.inboundReceipts.length !== 0) ? 
						<View style={{flexDirection: 'row'}}>
							<View style={{width: '50%'}}>
								<Text style={styles.headers}>Date Received:</Text>
								<Text style={styles.headers}>Item:</Text>
								<Text style={styles.headers}>SKU:</Text>
								<Text style={styles.headers}>Quantity:</Text>
							</View>
							<View style={{alignItems: "flex-end", width: '50%'}}>
								<Text style={styles.values}>{inbound.inboundReceipts[0].date}</Text>
								<Text style={styles.values}>{inbound.inboundReceipts[0].sku}</Text>
								<Text style={styles.values}>{inbound.inboundReceipts[0].name}</Text>
								<Text style={styles.values}>{`${inbound.inboundReceipts[0].quantity}`}</Text>
							</View>
						</View> : <Text style={{marginLeft: 10}}>No recent shipments...</Text>
						}
					</Card>
				</View>
				<TouchableOpacity onPress={() => navigation.navigate('Inventory', { screen: 'Inventory'})}>
				<Card title="Inventory Adjustments" titleStyle={{fontSize: 14}} containerStyle={[styles.card, { marginBottom: 10, marginRight: 15, marginLeft: 15}]}>
					<ScrollView style={{marginTop: 10}}>
						<FlatList
							data={items.itemQuantities}
							renderItem={({item}) => {
									return (
										<View style={{marginLeft: '2.5%',  marginRight: '2.5%', backgroundColor: item.quantity < 10 ? '#FF6961' : '#90EE90', borderWidth: 1, marginBottom: 5}}>
											<View style={{flexDirection: 'row', marginBottom: 10}}>
												<Image style={{ marginLeft: 15, marginRight: 20, marginTop: 10, width: 100, height: 100, marginTop: 15 }} source={require('../../../assets/images/sample-png.png')}/>
												<View style={{width: '25%'}}>
													<Text style={styles.headers}>Name:</Text>
													<Text style={styles.headers}>SKU:</Text>
													<Text style={styles.headers}>Units:</Text>
													<Text style={styles.headers}>Date:</Text>
												</View>
												<View style={{ marginRight: 10, alignItems: "flex-end", width: '33%'}}>
													<Text style={styles.values}>{item.name}</Text>
													<Text style={styles.values}>{item.sku}</Text>
													<Text style={styles.values}>{item.quantity}</Text>
													<Text style={styles.values}>{item.date ? displayDate(item.date) : 'N/A'}</Text>
												</View>
											</View>
											{/* <Entypo name="dot-single" color="green" style={{position: 'absolute', marginLeft: '85%' }} size={65}/> */}
											<Divider/>
										</View>
									)
								}}  
							keyExtractor={(item, index) => index}
							ListEmptyComponent={() => {
								return (
									<Text style={{marginLeft: 10}}>No items in inventory...</Text>
								)
							}}
						/>
					</ScrollView>
				</Card>
				</TouchableOpacity>
			</View>
		</SafeAreaView>
	)}


const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	},
	buttonContainer: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		flex: 1
	},
	container: {
		flex: 1,
		backgroundColor: '#F5FCFF',
		borderWidth: 1,
		flexDirection: 'column',
	},
	card: {
		marginLeft: 5,
		marginRight: 5 
	},
	values: {
		fontSize: 16,
		marginTop: 10
	},
	headers: {
		fontSize: 16,
		fontWeight: '600',
		marginTop: 10
	},
})

export default HomeScreen;