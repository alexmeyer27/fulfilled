import React, { Component, useState, useEffect } from 'react';
import { Text, StyleSheet, View, Image, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import { useNavigation, useLinkProps } from '@react-navigation/native'
import { Card, Button, Input, CheckBox } from 'react-native-elements';
import { useSelector, useDispatch } from 'react-redux';
import { additem, deleteitem, setcurrentitem, receiveintoinventory} from '../../redux/items/index';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import DropDownPicker from 'react-native-dropdown-picker';
import { launchImageLibrary } from 'react-native-image-picker';
import moment from 'moment';


import AntDesign from 'react-native-vector-icons/AntDesign'
import ActionButton from '../../components/ActionButton';
import { setitemreceiving } from '../../redux/inbound';

boxArray = [
	{label: 'test box 1', value: 1},
	{label: 'test box 2', value: 2}
]


function ItemCreationScreen({ route, navigation }) {

	const item = useSelector(state => state.items);
	const inbound = useSelector(state => state.inbound);
	const dispatch = useDispatch();
	const addItem = item => dispatch(additem(item));
	const deleteItem = id => dispatch(deleteitem(id));
	const setCurrentItem = item => dispatch(setcurrentitem(item));
	const receiveIntoInventory = (item, quantity) => dispatch(receiveintoinventory(item, quantity));
	const setItemReceiving = itemReceived => dispatch(setitemreceiving(itemReceived));

	const { scannedUpc, receiving, receivedItemCreate } = route.params;

	let importedUpc = scannedUpc ? scannedUpc.data : '';
	// let itemScanned = itemReceive ? true : false;

	const [itemEdited, setItemEdited] = useState(false);
	const [upc, setUpc] = useState(importedUpc);
	const [sku, setSku] = useState('');
	const [name, setName] = useState('');
	const [weight, setWeight] = useState('');
	const [length, setLength] = useState('');
	const [width, setWidth] = useState('');
	const [height, setHeight] = useState('');
	const [box, setBox] = useState('');
	const [cost, setCost] = useState('');
	const [image, setImage]  = useState('');
	const [itemReceiving, setReceiving] = useState(receiving);
	const [inboundSessionEnabled, setInboundSessionEnabled] = useState(inbound.inboundSession);
	const [receivingItemCreate, setReceivingItemCreate] = useState(receivedItemCreate)


	const makeid = (length) => {
		var result = '';
		var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		var charactersLength = characters.length;
		for ( var i = 0; i < length; i++ ) {
		   result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}


	const toggleInboundSessionEnabled = () => {
		setInboundSessionEnabled(!inboundSessionEnabled)
	};
	
	React.useLayoutEffect(() => {
		navigation.setOptions({
		  headerRight: () => (
			<TouchableOpacity onPress={() => {
				addItem({ upc, sku, name, weight, length, width, height, box, cost, image });
				let date = moment().format('M/D/YYYY hh:mm a');
				if (itemReceiving === true) {
					setCurrentItem({ upc, sku, name, weight, length, width, height, box, cost, image });
					receiveIntoInventory({sku: sku, name: name, date: date }, 0);
					navigation.navigate('Department', {screen: 'Inbound Receiving', params: {scannedUpc: '', itemQuantity: 0}});
				} else {
					receiveIntoInventory({sku: sku, name: name, date: date }, 0)
					navigation.navigate('Inventory');
				}
			}}>
				<Text style={{color: '#0F3F57', fontWeight: '800', marginRight: 10}}>
				Submit
				</Text>
			</TouchableOpacity>
		  ),
		});
	  }, [navigation, upc, sku, name, weight, length, width, height, box, cost, image, itemReceiving]);

	  useEffect(() => {		
		if (receivingItemCreate) {
			alert('Item not found, please create a new item');
			setReceivingItemCreate(false)
		}
	});

		return (
			<KeyboardAwareScrollView style={styles.container}>
				<SafeAreaView>
				<View style={{flexDirection: 'row', marginTop: 10, alignSelf: 'center'}}>
					<Button 
						title={"Generate SKU"} 
						containerStyle={{borderColor: '#0F3F57', marginRight: 10}} 
						titleStyle={{fontSize: 14, color: '#0F3F57'}} 
						type={"outline"}
						onPress={() => {
							setSku(makeid(8));
						}}
					/>
					<Button 
						title={"Import Item Template"} 
						containerStyle={styles.button} 
						titleStyle={{fontSize: 14, color: '#0F3F57'}} 
						type={"outline"}
						onPress={() => {
							navigation.navigate('Item Search')
						}}
					/>
				</View>
				<Card title={'Tracking'}>
					<Input 
						label={"UPC"} 
						onChangeText={value => setUpc(value)}
						value={upc}
						keyboardType={'number-pad'}
						labelStyle={styles.label}		
					/>
					<Input 
						label={"SKU"} 
						onChangeText={value => setSku(value)}
						value={sku}
						labelStyle={styles.label}				
					/>
				</Card>
				<Card title={'Item Details'}>	
					<ScrollView>
						<Input 
							label={"Item Name"} 
							onChangeText={value => setName(value)}
							value={name}	
							labelStyle={styles.label}		
						/>
						<View style={{flexDirection: 'row'}}>
							<Input 
								label={"Weight"} 
								onChangeText={value => setWeight(value)}
								keyboardType={'decimal-pad'}	
								value={weight}
								labelStyle={styles.label}		
							/>
						</View>
						<Input 
							label={"Length"} 
							onChangeText={value => setLength(value)}
							value={length}
							keyboardType={'decimal-pad'}	
							labelStyle={styles.label}
						/>
						<Input 
							label={"Width"} 
							onChangeText={value => setWidth(value)}
							value={width}
							keyboardType={'decimal-pad'}	
							labelStyle={styles.label}	
						/>
						<Input 
							label={"Height"} 
							onChangeText={value => setHeight(value)}
							value={height}
							keyboardType={'decimal-pad'}
							labelStyle={styles.label}		
						/>
						<View style={{padding: 10}}>
							<Text style={{color: '#0F3F57', fontWeight: 'bold', fontSize: 16, marginBottom: 15}}>Box</Text>
							<DropDownPicker
									items={boxArray}
									defaultValue={boxArray.testbox1}
									containerStyle={{height: 40}}
									style={{backgroundColor: '#fafafa'}}
									itemStyle={{
										justifyContent: 'flex-start'
									}}
									dropDownStyle={{backgroundColor: '#fafafa'}}
									onChangeItem={item => {
										setBox(item)
									}}
									labelStyle={{
										fontSize: 14,
										textAlign: 'left',
										color: '#0F3F57'
									}}
								/>
							</View>
						<Input 
							label={"Cost"} 
							onChangeText={value => setCost(value)}
							value={cost}	
							labelStyle={[styles.label, {marginTop: 15}]}
							keyboardType={'decimal-pad'}
						/>
						<TouchableOpacity style={{marginLeft: 10}} onPress={() => {launchImageLibrary({}, (response) => {
							setImage(response.uri)
						})}}>
							<AntDesign name="camera" size={32} />					
						</TouchableOpacity>
					</ScrollView>
				</Card>
				</SafeAreaView>		
			</KeyboardAwareScrollView>
		)
	}

const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	},
	container: {
		flex: 1,
		backgroundColor: '#F5FCFF',
		flexDirection: 'column',
	},
	button: {
		color: '#0F3F57',
		marginRight: 10
	},
	label: {
		color: '#0F3F57'
	}
})


export default ItemCreationScreen;
