import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, ScrollView } from 'react-native';
import { Tooltip, Text, SearchBar, Card, Button, Divider, Overlay } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import Feather from 'react-native-vector-icons/Feather';


class ItemSearchScreen extends React.Component {	
	state = {
		search: '',
	}
	
	searchItems = (search) => {
		this.setState({ search });
	};
	
	render() {
		const { search } = this.state;
		const { navigation } = this.props;

		return (
			<View style={styles.container}>
				<View style={{marginRight: 10, marginLeft: 10}}>
					<SearchBar
						placeholder="Search by item code..."
						onChangeText={this.updateSearch}
						value={search}
						inputStyle={styles.searchBar}
						// containerStyle={styles.buttonContainerStyle}
						// inputContainerStyle={styles.buttonContainer}
					/>		
				</View>
				<ScrollView>
				</ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	searchBar: {
		backgroundColor: '#fcf3db',
		borderRadius: 30,
		fontSize: 16,
		paddingHorizontal: 14,
		color: 'black'
	}
})


export default function(props) {
	const navigation = useNavigation();

	return <ItemSearchScreen {...props} navigation={navigation} />
}
