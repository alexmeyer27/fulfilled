import React, { Component } from 'react';
import { 
	Text, 
	Dimensions, 
	StyleSheet, 
	TouchableOpacity, 
	SafeAreaView, 
	View, 
	SectionList } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const { height, width } = Dimensions.get('window');

export default function OrdersScreen() {
	const navigation = useNavigation();
	
	return (
		<SafeAreaView>
			<View style={{marginTop: 10, width: '50%', alignSelf: 'center'}}>
				<View>
					<Text>Upcoming Orders</Text>
				</View>
				<View>
					<Text>Total Orders</Text>
				</View>
			</View>
		</SafeAreaView>
	)

}


const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	},
})