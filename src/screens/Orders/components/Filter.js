import React, { Component } from 'react';
import { 
	Text, 
	Dimensions, 
	StyleSheet, 
	SafeAreaView, 
	View,
	Picker
	} from 'react-native';
import { useNavigation } from '@react-navigation/native';
// import { Picker } from '@react-native-community/picker';

const { height, width } = Dimensions.get('window');

export default class FilterScreen extends React.Component {
	// const navigation = useNavigation();
	state = {
		carrier: ''
	}
	
	render() {
		return (
			<SafeAreaView>
				<View style={{alignItems: 'center', flex: 1}}>
					<Picker
						selectedValue={this.state.carrier}
						style={{height: 50, width: 100}}
						onValueChange={(itemValue, itemIndex) =>
							this.setState({language: itemValue})
						}>
						<Picker.Item label="UPS" value="ups" />
						<Picker.Item label="Fedex" value="fedex" />
					</Picker>
				</View>
			</SafeAreaView>
		)
	}
}


const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20
	},
})
