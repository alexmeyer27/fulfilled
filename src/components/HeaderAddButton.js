import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { Text, TouchableOpacity } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';



const HeaderAddButton = () => {
	const navigation = useNavigation();
	
	return (
		<TouchableOpacity onPress={() => {
			navigation.navigate('Item Creation', { scannedUpc: '', receiving: false });
		}}>
			<Text style={{color: '#0F3F57', fontWeight: '800', marginRight: 10}}>
			Add
			</Text>
		</TouchableOpacity>
	)
}

export default HeaderAddButton;