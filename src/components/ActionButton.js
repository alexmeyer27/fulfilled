import React from 'react'
import {
	TouchableOpacity, Text, View, StyleSheet
} from 'react-native'

const ActionButton = ({onPress, title}) => (
	<TouchableOpacity
		onPress={onPress}
		style={styles.buttonContainer}
		underlayColor='#ffbf2d'
	>
		<View style={styles.button}>
			<Text style={styles.buttonText}>{title}</Text>
		</View>
	</TouchableOpacity>
)

const styles = StyleSheet.create({
	buttonContainer: {
		backgroundColor: '#0F3F57',
		borderRadius: 25,
		marginLeft: '10%',
		marginRight: '10%'
 	},
	button: {
		height: 50,
		justifyContent: 'center',
		alignItems: 'center',
 	},
	buttonText: {
		color: 'white',
		fontSize: 18,
		margin: 10
  	}
})

export default ActionButton;