import React from 'react';
import { TouchableOpacity } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';


const FilterButton = () => {
	const navigation = useNavigation();
	
	return (
		<TouchableOpacity onPress={() => {
			navigation.navigate("Filter")
		}}>
			<FontAwesome5 name="filter" size={30} color="black" style={{marginRight: 15, justifyContent: 'center', alignItems: 'center'}}/>
		</TouchableOpacity>
	)
}

export default FilterButton;