import React from 'react';
import { Image, SafeAreaView, TouchableOpacity } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';

const BarcodeScanner = () => {
	const navigation = useNavigation();
	
	return (
		<TouchableOpacity onPress={() => {
			navigation.navigate("DepartmentScanner")
		}}>
			<MaterialCommunityIcons name="barcode-scan" color="black" style={{justifyContent: 'center', alignItems: 'center'}} size={75}/>
		</TouchableOpacity>
	)
}

export default BarcodeScanner