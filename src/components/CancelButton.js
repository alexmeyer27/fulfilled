import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { Text, TouchableOpacity } from 'react-native';


const CancelButton = () => {
	const navigation = useNavigation();
	
	return (
		<TouchableOpacity onPress={() => {
			navigation.goBack()
		}}>
			<Text style={{color: '#0F3F57', fontWeight: '800'}}>
			Cancel
			</Text>
		</TouchableOpacity>
	)
}

export default CancelButton;