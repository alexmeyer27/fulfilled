import React from 'react';
import { 
	Text, 
	Dimensions, 
	StyleSheet, 
	TouchableOpacity, 
	SafeAreaView, 
	View, 
	SectionList } from 'react-native';
import { Card, ListItem, Divider, Tooltip } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { State } from 'react-native-gesture-handler';


const OrderCard = () => {
	const navigation = useNavigation();
	
	return (
		<Card title={this.state.orderNumber} titleStyle={{fontSize: 14}} containerStyle={{maxHeight: '25%'}}>
			<View style={{flexDirection: 'row'}}>
				<TouchableOpacity>
					<Text>Order Number:</Text>
					<Text style={{color: '#0000EE'}}>Placeholder Order 12345</Text>
				</TouchableOpacity>
				<Text>Order Quantity: </Text>
				<Text>20</Text>
				<Text>Date Received: </Text>
				<Text>Sample Date</Text>
			</View>
		</Card>
	)
}

export default OrderCard;