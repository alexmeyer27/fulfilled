import { React } from 'react';
import { StyleSheet } from 'react-native';
import { Card } from 'react-native-elements';

const ListItem = ({ details }) => {
	return (
		<Card containerStyle={{
			borderRadius: 5,
			borderColor: '#0F3F57',
			elevation: 1
		}}>
			{details}
		</Card>
	)
}

export default ListItem;