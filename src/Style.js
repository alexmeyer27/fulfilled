import React from 'react';
import { View } from 'react-native';

export const Style = ({children}) => {
	return (
		<View style={{
			flex: 1,
			alignItems: "center",
			justifyContent: "center"
		}}>
		{children}
		</View>
	);
}